import re
from bisect import bisect
from datetime import time
from enum import Enum
from typing import Optional, List, Union

import httpx
from pydantic import BaseModel, Field

from src.config import APPROACHES, DEPARTURE_INFO, ARRIVAL_INFO, DEPARTURE_CONTACT, AIRPORTS_MAX_TRANSITION_LEVEL
from src.meteo.schemas import Airport, MetarSurfaceWind, MetarVisibility, MetarPresentWeather, MetarClouds, \
    MetarTemperatures


def transition_level(airport_icao: str, pressure: int) -> int:
    """Based on eAIP ENR 1.7.2 Basic altimeter setting procedures"""
    top_level = AIRPORTS_MAX_TRANSITION_LEVEL.get(airport_icao.lower())
    levels = [top_level - (5 * steps) for steps in range(8)]
    level_index = bisect([942.2, 959.5, 977.2, 995.1, 1013.3, 1031.7, 1050.3], pressure)
    return levels[-1] if level_index == len(levels) else levels[level_index]


def madeira_rmk_parser(text: str) -> dict[str, MetarSurfaceWind]:
    to_return = {}
    pattern = (
        r"(?P<position>\w+) "
        r"((?P<wind_direction>\d{3})|(?P<variable>VRB))"
        r"(?P<wind_intensity>\d{2})G?(?P<maximum>\d{2})?"
        r"(?P<unit>KT)( (?P<wind_variable_from>\d{3})V(?P<wind_variable_to>\d{3}))?"
    )
    matches = re.finditer(pattern, text)

    for match in matches:
        position = match.group("position").lower()
        to_return[position] = MetarSurfaceWind(
            wind_direction=match.group("wind_direction"),
            variable=bool(match.group("variable")),
            wind_speed=match.group("wind_intensity"),
            gust=bool(match.group("maximum")),
            maximum_wind_speed=match.group("maximum"),
            unit=match.group("unit"),
            wind_variable_from=match.group("wind_variable_from"),
            variability=bool(match.group("wind_variable_to")),
            wind_variable_to=match.group("wind_variable_to")
        )

    return to_return


class VatsimDataController(BaseModel):
    cid: int
    name: str
    callsign: str
    frequency: str
    facility: int
    rating: int
    server: str
    visual_range: int
    text_atis: Optional[list[str]]
    last_updated: str
    logon_time: str


class ATISType(str, Enum):
    DEPARTURE = "DEP"
    ARRIVAL = "ARR"
    GENERAL = ""


# --- ATIS Model ---
class Atis(BaseModel):
    airport_icao: str = Field(min_length=4, max_length=4)
    atis_type: ATISType
    letter: str = Field(min_length=1, max_length=1)
    atis_time: time
    lvo: bool = False
    approach_type_message: Optional[str] = None
    runway: str = Field(min_length=2, max_length=2)
    transition_level: int = Field(ge=40, le=115)
    special_messages: List[Optional[str]] = []
    static_messages: List[Optional[str]] = []
    wind: MetarSurfaceWind
    cavok: bool = False
    visibility: MetarVisibility
    present_weather: List[Optional[MetarPresentWeather]] = []
    cloud_coverage: List[Optional[MetarClouds]] = []
    vertical_visibility: Optional[int]
    temperatures: MetarTemperatures
    remarks: Optional[str] = None
    pressure: int = Field(ge=890, le=1075)

    @staticmethod
    def parse_visibility(value: int) -> str:
        if value >= 9999:
            return "10KM"
        elif 5000 < value < 9999:
            return f"{int(value / 1000)}KM"
        else:
            return f"{value}M"

    def message(self) -> str:
        """Generate the textual ATIS message."""
        lines = []
        lines.append(f"{self.airport_icao} {self.atis_type.value} ATIS {self.letter}")

        # Time & runway
        if self.atis_type == ATISType.DEPARTURE:
            lines.append(f"{self.atis_time.strftime('%H%M')}Z RWY IN USE {self.runway:02}")
        elif self.airport_icao.lower() == "lppr" and self.lvo:
            lines.append(f"{self.atis_time.strftime('%H%M')}Z LOW VIS PROCEDURES IN FORCE")
            lines.append("ILS CAT 2 APCH")
            lines.append(f"RWY IN USE {self.runway:02}")
        elif self.approach_type_message:
            lines.append(f"{self.atis_time.strftime('%H%M')}Z {self.approach_type_message} RWY IN USE {self.runway:02}")
        else:
            lines.append(f"{self.atis_time.strftime('%H%M')} RWY IN USE {self.runway:02}")

        # TRL
        lines.append(f"TRL {self.transition_level}")

        # LVO
        if self.airport_icao.lower() != "lppr" and self.lvo:
            lines.append("LOW VIS PROCEDURES IN FORCE")

        # Static and special messages
        lines.extend(filter(None, self.static_messages))
        lines.extend(filter(None, self.special_messages))

        # Wind
        wind_position = ""
        if self.airport_icao.lower() == "lpma":
            if self.atis_type == ATISType.ARRIVAL:
                wind_position = "TDZ "
            elif self.atis_type == ATISType.DEPARTURE:
                wind_position = "MID "

        if self.wind.variable:
            lines.append(f"WND {wind_position}VRB {self.wind.wind_speed} KT")
        else:
            wind_line = (
                f"WND {wind_position}{self.wind.wind_direction:03} DEG "
                f"{self.wind.wind_speed} KT"
            )
            if self.wind.gust:
                wind_line += f" MAX {self.wind.maximum_wind_speed} KT"
            if self.wind.wind_variable_from and self.wind.wind_variable_to:
                wind_line += (
                    f" VRB BTN {self.wind.wind_variable_from:03} DEG "
                    f"AND {self.wind.wind_variable_to:03} DEG"
                )
            lines.append(wind_line)

        # Visibility / Weather
        if self.cavok:
            lines.append("CAVOK")
        else:
            lines.append(f"VIS {self.parse_visibility(self.visibility.prevailing_visibility)}")

        if self.present_weather:
            weather_line_parts = []
            intensity_map = {"-": "FBL", "+": "HVY"}
            for weather in self.present_weather:
                if weather.intensity == "VC":
                    continue
                if weather.intensity in intensity_map:
                    weather_line_parts.append(intensity_map[weather.intensity])
                if weather.obscuration:
                    weather_line_parts.append(weather.obscuration)
            if weather_line_parts:
                lines.append(" ".join(weather_line_parts))

        # Clouds
        if self.cloud_coverage:
            cloud_line = "CLD " + " ".join(
                f"{layer.cloud_amount} {layer.height_of_base}FT"
                for layer in self.cloud_coverage
            )
            lines.append(cloud_line)

        # Vertical visibility
        if self.vertical_visibility:
            lines.append(f"VER VIS {self.vertical_visibility}FT")

        # Temps & Pressure
        lines.append(
            f"TEMP {self.temperatures.air_temperature} "
            f"DP {self.temperatures.dew_point_temperature} "
            f"QNH {self.pressure}"
        )

        # LPMA Remarks (Rosario wind, etc)
        if self.airport_icao.lower() == "lpma" and self.remarks:
            madeira_winds = madeira_rmk_parser(self.remarks)
            rosario = madeira_winds.get("rosario")
            if rosario:
                rosario_line = "RS "
                if rosario.variable:
                    rosario_line += f"WND VRB {rosario.wind_speed} KT"
                else:
                    wind_line = (
                        f"WND {rosario.wind_direction:03} DEG {rosario.wind_speed} KT"
                    )
                    if rosario.gust:
                        wind_line += f" MAX {rosario.maximum_wind_speed} KT"
                    if rosario.wind_variable_from and rosario.wind_variable_to:
                        wind_line += (
                            f" VRB BTN {rosario.wind_variable_from:03} DEG "
                            f"AND {rosario.wind_variable_to:03} DEG"
                        )
                    rosario_line += wind_line
                lines.append(rosario_line)

        # End
        lines.append(f"ACK {self.atis_type.value} INFO {self.letter}")

        return "\n".join(line.upper() for line in lines)


# --- Main Public Function(s) ---
def fetch_airport_data_from_icao(icao: str) -> Airport:
    """Fetch airport data given an ICAO code."""
    response = httpx.get(f"https://meteo.vatsim.pt/api/v1/airport?airport_icao_codes={icao}")
    response.raise_for_status()
    data = response.json()
    return Airport(**data[0])  # The API returns a list with a single item


def fetch_airport_data_from_metar(metar: str) -> Airport:
    """Fetch airport data given a raw METAR."""
    response = httpx.get(f"https://meteo.vatsim.pt/api/v1/custom/airport?metar={metar}")
    response.raise_for_status()
    data = response.json()
    return Airport(**data)


def fetch_controllers() -> List[VatsimDataController]:
    """Optionally fetch controllers info."""
    response = httpx.get("https://vatdata.vatsim.pt/api/v1/live/controllers/lp")
    response.raise_for_status()
    data = response.json()
    return [VatsimDataController(**ctrl) for ctrl in data]


def build_atis_object(
        *,
        airport: Airport,
        atis_type: ATISType,
        letter: str,
        runway: Optional[str],
        lvo: bool,
        show_freqs: bool
) -> Atis:
    """
    Build an Atis model from an Airport data model and user options.
    This is a pure function that doesn't do any I/O.
    """
    runway_selected = runway or airport.optimal_runways[0]
    approach_type_message = APPROACHES.get(airport.icao.lower(), {}).get(runway_selected.strip(), "")

    # Prepare static & special messages
    static_messages: List[str] = []
    special_messages: List[str] = []

    # Add static info
    def add_static_message(msg_dict):
        static_messages.append(msg_dict.get(airport.icao.lower(), {}).get(runway_selected, ""))

    if atis_type == ATISType.DEPARTURE:
        add_static_message(DEPARTURE_INFO)
    elif atis_type == ATISType.ARRIVAL:
        add_static_message(ARRIVAL_INFO)
    else:  # GENERAL
        add_static_message(DEPARTURE_INFO)
        add_static_message(ARRIVAL_INFO)

    # If frequencies are needed
    if show_freqs:
        controllers = fetch_controllers()  # side effect
        online_frequencies = [controller.frequency for controller in controllers]
        departure_messages = DEPARTURE_CONTACT.get(airport.icao.lower())
        static_messages.extend([msg[1] for msg in departure_messages if msg[0] in online_frequencies])

    atis = Atis(
        airport_icao=airport.icao,
        atis_type=atis_type,
        letter=letter,
        lvo=lvo,
        transition_level=transition_level(
            airport_icao=airport.icao,
            pressure=airport.metar.pressure
        ),
        atis_time=airport.metar.identification.issued.time(),  # If it's a datetime, use .time() as needed
        approach_type_message=approach_type_message,
        runway=runway_selected,
        special_messages=special_messages,
        static_messages=static_messages,
        wind=airport.metar.surface_wind,
        cavok=airport.metar.cavok,
        present_weather=airport.metar.present_weather,
        remarks=airport.metar.remarks,
        visibility=airport.metar.visibility,
        cloud_coverage=airport.metar.clouds,
        vertical_visibility=airport.metar.vertical_visibility,
        temperatures=airport.metar.temperatures,
        pressure=airport.metar.pressure
    )
    return atis


def atis_generator(
        atis_type: ATISType = ATISType.GENERAL,
        metar: Optional[str] = None,
        icao: Optional[str] = None,
        runway: Optional[str] = None,
        info: str = "A",
        show_freqs: bool = False,
        lvo: bool = False
) -> Union[str, None]:
    """
    Public function that:
    - Validates input
    - Fetches the data (ICAO or METAR)
    - Builds the ATIS object
    - Returns the final string
    """

    # Input validations
    if not (metar or icao):
        return "ATIS OUT OF SERVICE - YOU NEED TO SUPPLY AN ICAO OR METAR"
    if metar and icao:
        return "ATIS OUT OF SERVICE - SUPPLY ONLY ONE ICAO OR ONLY ONE METAR"
    if icao and (len(icao) != 4 or not icao.isalpha() or not icao.isupper()):
        return "ATIS OUT OF SERVICE - SUPPLY A VALID ICAO (4 UPPERCASE LETTERS)"
    if icao and icao.lower() == "lpla" and atis_type != ATISType.GENERAL:
        return "ATIS OUT OF SERVICE - LAJES ATIS TYPE RESTRICTED TO GENERAL"

    # Fetch data
    try:
        if icao:
            airport = fetch_airport_data_from_icao(icao)
        else:
            airport = fetch_airport_data_from_metar(metar)  # type: ignore
    except httpx.HTTPError as err:
        return f"ATIS OUT OF SERVICE - METEOAPI ERROR: {err}"

    # Build ATIS object
    atis_obj = build_atis_object(
        airport=airport,
        atis_type=atis_type,
        letter=info,
        runway=runway,
        lvo=lvo,
        show_freqs=show_freqs
    )

    # Generate the message
    atis_message = atis_obj.message()
    # You might want to unify spacing, newlines, etc.
    return " ".join(atis_message.split())
