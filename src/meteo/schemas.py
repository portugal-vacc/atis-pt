from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class MetarIdentification(BaseModel):
    corrected: bool = False
    location: str
    automatic: bool = False
    issued: datetime
    outdated: bool = False
    refer_to_location: Optional[str]


class MetarSurfaceWind(BaseModel):
    wind_direction: Optional[int] = None
    variable: bool = False
    wind_speed: int = None
    gust: bool = False
    maximum_wind_speed: Optional[int] = None
    unit: str = None
    wind_variable_from: Optional[int] = None
    variability: bool = False
    wind_variable_to: Optional[int] = None


class MetarVisibility(BaseModel):
    prevailing_visibility: Optional[int]
    lowest_visibility: Optional[int]
    lowest_visibility_direction: Optional[int]


class MetarRunwayVisualRange(BaseModel):
    runway: str
    modifier: Optional[str]
    value: int
    tendency: Optional[str]


class MetarPresentWeather(BaseModel):
    intensity: Optional[str]
    descriptor: Optional[str]
    precipitation: Optional[str]
    obscuration: Optional[str]
    other: Optional[str]


class MetarClouds(BaseModel):
    cloud_amount: str
    height_of_base: Optional[str]
    cloud_type: Optional[str]


class MetarTemperatures(BaseModel):
    air_temperature: int
    dew_point_temperature: int


class WindShear(BaseModel):
    runway: Optional[int]
    all_runways: bool = False


class Metar(BaseModel):
    identification: MetarIdentification
    surface_wind: MetarSurfaceWind
    visibility: Optional[MetarVisibility]
    runway_visual_range: list[Optional[MetarRunwayVisualRange]]
    present_weather: list[Optional[MetarPresentWeather]]
    clouds: list[Optional[MetarClouds]]
    vertical_visibility: Optional[int]
    cavok: bool = False
    temperatures: MetarTemperatures
    pressure: int | str
    wind_shear: list[Optional[WindShear]]
    trend_forecast: Optional[str]
    remarks: Optional[str]
    raw: str


class WindComponent(BaseModel):
    runway: str
    wind_speed: int
    wind_direction: int
    angle_difference: int
    head_wind: float
    cross_wind: float


class Airport(BaseModel):
    icao: str
    metar: Metar = None
    runways: list = None
    optimal_runways: list[str]
    low_visibility_ops: bool
    wind_components: list[Optional[WindComponent]]
