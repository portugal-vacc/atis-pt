from typing import Optional

from fastapi import APIRouter
from starlette.responses import PlainTextResponse

from src.generator import atis_generator, ATISType
from src.logging import get_logger

logger = get_logger(__name__)
generator_api = APIRouter()


@generator_api.get("/generate", response_class=PlainTextResponse)
def main(atis_type: ATISType = ATISType.GENERAL, metar: Optional[str] = None, icao: Optional[str] = None,
         runway: Optional[str] = None, info: str = "A", show_freqs: bool = True, lvo: bool = False):
    return atis_generator(atis_type, metar, icao, runway, info, show_freqs, lvo)
