APPROACHES = dict(lppt={"02": "EXP ILS APCH", "20": "EXP ILS APCH"},
                  lpfr={'10': 'EXP ILS Z APCH', '28': 'EXP ILS Z APCH'},
                  lppr={'17': 'ILS DME APCH', '35': 'RNP APCH'},
                  lpma={'05': 'EXP RNP APP A', '23': 'EXP RNP APP B'},
                  lppd={'12': 'EXP RNP APCH', '30': 'EXP ILS APCH'},
                  lpla={'15': 'EXP ILS APCH', '33': 'EXP ILS APCH'})

AIRPORTS_MAX_TRANSITION_LEVEL = dict(lpcs=75, lpfr=75, lppt=75, lppr=75, lpbj=75, lpev=75, lpaz=75, lpfl=85, lphr=85,
                                     lpla=85, lppi=85, lpgr=85, lpsj=85, lpcr=85, lpma=85, lpps=85, lppd=95, lpvr=115,
                                     lpbg=115)

ARRIVAL_INFO = dict(lppt={
    '02': 'AFTER LDG VACATE VIA H" 4',
    '20': 'AFTER LDG VACATE VIA H" 1 OR H" 3',
})

DEPARTURE_INFO = dict(
    lppt={
        '20': 'MEDIUM AND LGT AIRCRAFT EXP POSITION U" FOR DEP TKOF AVBL DIST 2412 M IF UNABLE ADVISE BEFORE PUSH',
    },
    lpla={
        '15': ['IN CASE OF GO AROUND FLY RWY HDG CLIMB TO 3000 FT CAUTION HIGH TERRAIN BOTH SIDES OF RWY'],
        '33': ['CAUTION HIGH TERRAIN BOTH SIDES OF RWY']
    })

# Em principio não é preciso meter isto noutro saco.
# SPECIAL_CONTACT = dict(
#     lppt=[('119.100', 'AFTER DEP CTC 119.1'),
#           ('136.020', 'AFTER DEP CTC 136.025'),
#           ('132.850', 'AFTER DEP CTC 132.850'),
#           ('125.550', 'AFTER DEP CTC 125.550')]
# )

DEPARTURE_CONTACT = dict(
    lppt=[
        ('118.955', 'FIRST CTC ON DEL FREQ 118.955'),
        ('121.755', 'FIRST CTC ON GND FREQ 121.755'),
        ('118.105', 'FIRST CTC ON TWR FREQ 118.105'),
        ('119.105', 'FIRST CTC ON APP FREQ 119.105'),
        ('136.030', 'FIRST CTC FREQ 136.030'),
        ('132.850', 'FIRST CTC FREQ 132.850'),
        ('125.550', 'FIRST CTC FREQ 125.550')],
    lppr=[
        ('118.930', 'DELIVERY 118.930'),
        ('121.040', 'GND FREQ 121.040 DELIVERY CL'),
        ('118.005', 'PORTO DEL CL'),
        ('120.910', 'DELIVERY CL TOWER AND APCH FREQ 120.910'),
        ('132.305', 'ON THE GROUND CTC 132.305'),
        ('132.850', 'ON THE GROUND CTC 132.850'),
        ('125.550', 'ON THE GROUND CTC 125.550'),
    ],
    lpfr=[
        ('118.580', 'FOR AIR TRAFIC CONTROL CLEARANCE CTC GND 118.580'),
        ('120.755', 'GND CLOSED'),
        ('119.405', 'ON THE GROUND CTC APP 119.405'),
        ('132.705', 'ON THE GROUND CTC 132.705'),
        ('132.855', 'ON THE GROUND CTC 132.855'),
        ('125.550', 'ON THE GROUND CTC 125.550'),
    ],
    lpma=[
        ('124.660', 'ON THE GROUND CTC TWR 124.660'),
        ('119.605', 'ON THE GROUND CTC APP 119.605'),
        ('132.255', 'ON THE GROUND CTC 132.255'),
        ('131.325', 'ON THE GROUND CTC 131.325'),
        ('125.550', 'ON THE GROUND CTC 125.550'),
    ],
    lppd=[
        ('118.300', ''),
        ('119.400', 'ON THE GROUND CTC APP 119.400'),
        ('132.150', 'ON THE GROUND CTC 132.150'),
        ('133.060', 'ON THE GROUND CTC 133.060')
    ],
    lpla=[
        ('134.100', ''),
        ('121.900', ''),
        ('122.100', 'ON THE GROUND CTC TWR 122.100'),
        ('135.000', 'ON THE GROUND CTC APP 135.000'),
        ('132.150', 'ON THE GROUND CTC 132.150'),
        ('133.060', 'ON THE GROUND CTC 133.060'),
    ]
)
