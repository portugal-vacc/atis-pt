from datetime import datetime
from enum import Enum


class ATISType(str, Enum):
    DEPARTURE = "DEP"
    ARRIVAL = "ARR"
    DEPARTURE_ARRIVAL = "SOMETHING"  # I don't know


def parse_visibility(value: int) -> str:
    if value >= 9999:
        return "10KM"
    elif 5000 < value < 9999:
        return f"{int(value / 1000)}KM"
    elif value <= 5000:
        return f"{value}M"


def main():
    airport = "LPPT"
    type = ATISType.DEPARTURE
    letter = "G"
    time = datetime.utcnow()
    expect_approach = "EXP ILS APCH"
    runway = "02"
    transition_level = 55
    after_landing = "AFTER LDG VACATE VIA H4"
    wind_heading = 70
    wind_intensity = 9
    visibility = 8500
    temperature = 13
    dew_point = 12
    qnh = 1005

    output = str()
    output += f"{airport} {type.value} {letter} {time.strftime('%H%M')}Z\n"
    output += f"{expect_approach}\n"
    output += f"RWY IN USE {runway}\n"
    output += f"TRL {transition_level}\n"
    output += f"{after_landing}\n"
    output += f"WND TDZ {wind_heading:03} DEG {wind_intensity} KT\n"
    output += f"VIS {parse_visibility(visibility)}\n"
    output += f"TEMP {temperature} DP {dew_point} QNH {qnh}\n"
    output += f"ACK {type.value} INFO {letter}\n"
    return output.upper()


if __name__ == "__main__":
    print(main())
