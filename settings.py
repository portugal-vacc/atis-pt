from dotenv import load_dotenv
from pydantic import BaseSettings

load_dotenv()


class Settings(BaseSettings):
    debug: bool = False

    api_port: int = 9415

    config_file_location: str = "config.json"

    window_update_active: bool = True
    window_update_interval: int = 1  # Minutes

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'


settings = Settings()
