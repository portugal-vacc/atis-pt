from src.generator import atis_generator, ATISType


def test_1():
    message = atis_generator(atis_type=ATISType.GENERAL, metar="LPPT 291500Z 29007KT 230V340 CAVOK 27/07 Q1021",
                             icao=None, runway="02", info="A", show_freqs=False, lvo=False)
    assert message == ("LPPT ATIS A 1500Z EXP ILS APCH RWY IN USE 02 TRL 50 AFTER LDG VACATE VIA H4"
                       " WND 290 DEG 7 KT VRB BTN 230 DEG AND 340"
                       " DEG CAVOK TEMP 27 DP 7 QNH 1021 ACK INFO A")


def test_2():
    message = atis_generator(atis_type=ATISType.GENERAL, metar="LPPT 291500Z 29007KT 230V340 CAVOK 27/07 Q1021",
                             icao=None, runway="20", info="A", show_freqs=False, lvo=False)
    assert message == ("LPPT ATIS A 1500Z EXP ILS APCH RWY IN USE 20 TRL 50"
                       " MEDIUM AND LGT AIRCRAFT EXP POSITION U FOR DEP TKOF AVBL DIST 2412 M"
                       " IF UNABLE ADVISE BEFORE PUSH AFTER LDG VACATE VIA H1 ADVISE IF UNABLE"
                       " WND 290 DEG 7 KT VRB BTN 230 DEG AND 340 DEG CAVOK TEMP 27 DP 7 QNH 1021 ACK INFO A")
