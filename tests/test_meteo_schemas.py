from datetime import datetime

import pytest
from pydantic import ValidationError

from src.meteo.schemas import (
    MetarIdentification,
    MetarSurfaceWind,
    MetarVisibility,
    MetarRunwayVisualRange,
    MetarPresentWeather,
    MetarClouds,
    MetarTemperatures,
    WindShear,
    Metar,
    WindComponent,
    Airport,
)


# -----------------------------------------------------
# MetarIdentification Tests
# -----------------------------------------------------
def test_metar_identification_valid():
    identification = MetarIdentification(
        corrected=True,
        location="LPMA",
        automatic=False,
        issued=datetime(2023, 1, 15, 12, 30, 0),
        outdated=False,
        refer_to_location=None
    )
    assert identification.location == "LPMA"
    assert identification.corrected is True
    assert identification.issued == datetime(2023, 1, 15, 12, 30, 0)


def test_metar_identification_missing_location():
    # 'location' is required, so we expect ValidationError
    with pytest.raises(ValidationError):
        MetarIdentification(
            corrected=True,
            issued=datetime(2023, 1, 15, 12, 30, 0)
        )


# -----------------------------------------------------
# MetarSurfaceWind Tests
# -----------------------------------------------------
def test_metar_surface_wind_valid():
    wind = MetarSurfaceWind(
        wind_direction=360,
        variable=False,
        wind_speed=10,
        gust=True,
        maximum_wind_speed=20,
        unit="KT",
        wind_variable_from=None,
        variability=False,
        wind_variable_to=None
    )
    assert wind.wind_direction == 360
    assert wind.unit == "KT"


def test_metar_surface_wind_missing_speed():
    # 'wind_speed' is not explicitly marked as Optional (defaults to None),
    # so if we pass nothing, it should still instantiate but be None.
    wind = MetarSurfaceWind()
    assert wind.wind_speed is None
    # But we might want to ensure speed is an int if given
    with pytest.raises(ValidationError):
        MetarSurfaceWind(wind_speed="FAST")  # invalid type


# -----------------------------------------------------
# MetarVisibility Tests
# -----------------------------------------------------
def test_metar_visibility_valid():
    visibility = MetarVisibility(
        prevailing_visibility=8000,
        lowest_visibility=6000,
        lowest_visibility_direction=180
    )
    assert visibility.prevailing_visibility == 8000


def test_metar_visibility_none_ok():
    # All fields are Optional, so this is valid:
    visibility = MetarVisibility()
    assert visibility.prevailing_visibility is None


# -----------------------------------------------------
# MetarRunwayVisualRange Tests
# -----------------------------------------------------
def test_metar_runway_visual_range_valid():
    rvr = MetarRunwayVisualRange(
        runway="09",
        modifier="M",
        value=300,
        tendency="U"
    )
    assert rvr.runway == "09"
    assert rvr.value == 300


def test_metar_runway_visual_range_missing_runway():
    with pytest.raises(ValidationError):
        MetarRunwayVisualRange(value=300)


# -----------------------------------------------------
# MetarPresentWeather Tests
# -----------------------------------------------------
def test_metar_present_weather_valid():
    weather = MetarPresentWeather(
        intensity="-",
        descriptor="BC",
        precipitation="RA",
        obscuration=None,
        other=None
    )
    assert weather.intensity == "-"


def test_metar_present_weather_empty():
    # All fields are optional
    weather = MetarPresentWeather()
    assert weather.intensity is None


# -----------------------------------------------------
# MetarClouds Tests
# -----------------------------------------------------
def test_metar_clouds_valid():
    clouds = MetarClouds(
        cloud_amount="BKN",
        height_of_base="015",  # e.g., 1,500 ft
        cloud_type=None
    )
    assert clouds.cloud_amount == "BKN"


def test_metar_clouds_missing_amount():
    with pytest.raises(ValidationError):
        MetarClouds(height_of_base="015")


# -----------------------------------------------------
# MetarTemperatures Tests
# -----------------------------------------------------
def test_metar_temperatures_valid():
    temps = MetarTemperatures(
        air_temperature=15,
        dew_point_temperature=7
    )
    assert temps.air_temperature == 15
    assert temps.dew_point_temperature == 7


def test_metar_temperatures_missing_fields():
    with pytest.raises(ValidationError):
        MetarTemperatures()


# -----------------------------------------------------
# WindShear Tests
# -----------------------------------------------------
def test_wind_shear_valid():
    ws = WindShear(
        runway=9,
        all_runways=False
    )
    assert ws.runway == 9
    assert not ws.all_runways


def test_wind_shear_no_runway():
    ws = WindShear(all_runways=True)
    assert ws.runway is None
    assert ws.all_runways is True


# -----------------------------------------------------
# Metar Tests
# -----------------------------------------------------
def test_metar_valid():
    """Minimal valid Metar object."""
    metar_obj = Metar(
        identification=MetarIdentification(
            corrected=False,
            location="LPMA",
            issued=datetime(2023, 1, 15, 12, 30, 0)
        ),
        surface_wind=MetarSurfaceWind(
            wind_direction=180,
            wind_speed=12,
            unit="KT"
        ),
        visibility=MetarVisibility(prevailing_visibility=9999),
        runway_visual_range=[],
        present_weather=[],
        clouds=[],
        vertical_visibility=None,
        cavok=False,
        temperatures=MetarTemperatures(air_temperature=20, dew_point_temperature=10),
        pressure=1013,
        wind_shear=[],
        trend_forecast=None,
        remarks="NOSIG",
        raw="METAR LPMA 151230Z 18012KT CAVOK 20/10 Q1013 NOSIG"
    )
    assert metar_obj.identification.location == "LPMA"
    assert metar_obj.surface_wind.wind_speed == 12
    assert metar_obj.pressure == 1013


def test_metar_missing_identification():
    """Test missing required sub-model (MetarIdentification)."""
    with pytest.raises(ValidationError):
        Metar(
            surface_wind=MetarSurfaceWind(wind_speed=10),
            visibility=MetarVisibility(prevailing_visibility=9999),
            runway_visual_range=[],
            present_weather=[],
            clouds=[],
            vertical_visibility=None,
            cavok=False,
            temperatures=MetarTemperatures(air_temperature=20, dew_point_temperature=10),
            pressure=1013,
            wind_shear=[],
            trend_forecast=None,
            remarks=None,
            raw="Some METAR raw data"
        )


# -----------------------------------------------------
# WindComponent Tests
# -----------------------------------------------------
def test_wind_component_valid():
    comp = WindComponent(
        runway="09",
        wind_speed=10,
        wind_direction=100,
        angle_difference=10,
        head_wind=9.85,
        cross_wind=1.74
    )
    assert comp.runway == "09"
    assert comp.head_wind == 9.85


def test_wind_component_missing_fields():
    with pytest.raises(ValidationError):
        WindComponent(runway="09")


# -----------------------------------------------------
# Airport Tests
# -----------------------------------------------------
def test_airport_valid():
    """Test a minimal valid Airport model."""
    metar_obj = Metar(
        identification=MetarIdentification(
            corrected=False,
            location="LPMA",
            issued=datetime(2023, 1, 15, 12, 30, 0)
        ),
        surface_wind=MetarSurfaceWind(
            wind_direction=180,
            wind_speed=12,
            unit="KT"
        ),
        visibility=MetarVisibility(prevailing_visibility=9999),
        runway_visual_range=[],
        present_weather=[],
        clouds=[],
        vertical_visibility=None,
        cavok=False,
        temperatures=MetarTemperatures(air_temperature=20, dew_point_temperature=10),
        pressure=1013,
        wind_shear=[],
        trend_forecast=None,
        remarks=None,
        raw="METAR LPMA 151230Z 18012KT CAVOK 20/10 Q1013 NOSIG"
    )

    airport = Airport(
        icao="LPMA",
        metar=metar_obj,
        runways=["10", "28"],
        optimal_runways=["10"],
        low_visibility_ops=False,
        wind_components=[]
    )
    assert airport.icao == "LPMA"
    assert airport.metar.surface_wind.wind_speed == 12
    assert airport.optimal_runways == ["10"]


def test_airport_missing_icao():
    with pytest.raises(ValidationError):
        Airport(
            metar=None,
            runways=["10", "28"],
            optimal_runways=["10"],
            low_visibility_ops=False,
            wind_components=[]
        )
